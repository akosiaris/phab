import argparse
import re
import sys
from datetime import datetime
from lib import Client
from termcolor import colored
from pprint import pprint


class PhabClient(Client):
    def getTasksWithProject(self, project_phid, continue_=None, statuses=None, column_phids=None):
        r = self._getTasksWithProjectContinue(
            project_phid, continue_, statuses=statuses, column_phids=column_phids)
        cursor = r['cursor']
        for case in r['data']:
            if case['type'] != 'TASK':
                continue
            yield case['phid']
        if cursor.get('after'):
            for case in self.getTasksWithProject(
                    project_phid, cursor['after'], statuses=statuses, column_phids=column_phids):
                yield case

    def _getTasksWithProjectContinue(self, project_phid, continue_=None, statuses=None, column_phids=[]):
        params = {
            'limit': 100,
            'constraints': {
                'projects': [project_phid],
                'columnPHIDs': column_phids
            },
            "order": "updated"
        }
        if continue_:
            params['after'] = continue_
        if statuses:
            params['constraints']['statuses'] = statuses
        return self.post('maniphest.search', params)

    def moveColumns(self, task_phid, to_column):
        self.post('maniphest.edit', {
            'objectIdentifier': task_phid,
            'transactions': [{
                'type': 'column',
                'value': to_column,
            }]
        })


# TODO: put creds in a file
KEY = ''
DEFAULT_PROJECT = 'serviceops'
URL = 'https://phabricator.wikimedia.org'
USERNAME = 'jijiki'


def demojify(text):
    r_pattern = re.compile(pattern="["
                                   u"\U0001F600-\U0001F64F"  # emoticons
                                   u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                   u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                   u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                   u"\U00002700-\U000027BF"  # Dingbats
                                   u"\U0001F9F9"  # broom
                                   u"\U0001F9D8"  # yoga
                                   u"\U0001F94C"  # curling
                                   u"\U00002640"  # sign
                                   u"\U0000200D"  # Zero with joiner
                                   u"\U0001F94B"  # karate
                                   "⎈"  # helm
                                   "]+", flags=re.UNICODE)
    return r_pattern.sub(r'', text)


def getTasks(client, project_phid, column_phid):
    return


# map column PHIDs to human names
# TODO: make case insensitive
def mapColumns(client, project_phid):
    columns = client.getColumns(project_phid)
    mapping = {}
    for i in columns['data']:
        mapping[demojify(i['fields']['name']).replace(" ", "")] = i['phid']
    return mapping


# returns strings of defined length, is string is shorter, it simply adds spaces
def normaliseLength(string_, length=72):
    if len(string_) >= length:
        return string_[:length]
    else:
        return string_.ljust(length, ' ')


def main():
    # TODO: KeyboardInterrupt
    parser = argparse.ArgumentParser()
    parser.add_argument('command', nargs='*', type=str)
    args = parser.parse_args()
    client = PhabClient(URL, USERNAME, KEY)
    # TODO: override with --project,-p flag
    project_phid = client.lookupPhid('#' + DEFAULT_PROJECT)
    # get a projects column map
    project_columns = mapColumns(client, project_phid)
    column_phid = []
    # TODO: Add --order, -o, order by
    # TODO: limit results
    if 'ls' in args.command:
        try:
            if args.command[1]:
                column_ = args.command[1].replace(" ", "")
                column_phid = [project_columns[column_]]
                for task_phid in client.getTasksWithProject(
                        project_phid,
                        statuses=['open', 'stalled'],
                        column_phids=column_phid
                ):
                    task = client.taskDetails(task_phid)
                    print(
                        colored(normaliseLength(task['priority'], 12), 'yellow'),
                        colored(normaliseLength(task['objectName'], 8), 'white', 'on_grey'),
                        normaliseLength(task['title']),
                        colored(task['uri'], 'blue'),
                        colored(f"Last Updated:" +
                                f"{datetime.fromtimestamp(int(task['dateModified'])).strftime('%Y-%m-%d %H:%M')} UTC",
                                'magenta')
                    )
        except IndexError:
            for name, column_phid in mapColumns(client, project_phid).items():
                print(name)
        except KeyError:
            print(f'Column {args.command[1]} does not exist')
        except KeyboardInterrupt:
            sys.exit(1)

    if 'mv' in args.command:
        # TODO: try/except KeyError
        task_phid = client.lookupPhid(args.command[1])
        column_ = args.command[2].replace(" ", "")
        to_column_phid = [project_columns[column_]]
        client.moveColumns(task_phid,to_column_phid)
        print(f"Moved {args.command[1]} to {args.command[2]}")

        # TODO: view tags of task as well as author and assignee
    if 'view' in args.command:
        task_phid = client.lookupPhid(args.command[1])
        task_info = client.taskDetails(task_phid)
        print(colored(f"{task_info['objectName']} {task_info['title']}", 'yellow'))
        print(client.taskDetails(task_phid)['description'])


if __name__ == '__main__':
    sys.exit(main())

