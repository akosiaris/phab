# phab

Phabricator command line client

Available commands:

* `phab.py ls`
* `phab.py ls <column>`
* `phab view T<task id>`
* `phab mv T<task id> <column>`

For the time being, actions are being performed only on DEFAULT_PROJECT, and KEY is hardcoded. 
